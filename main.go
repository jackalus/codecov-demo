package main

import "fmt"

// Hello is a function that returns "Hello, World"
func Hello() string {
	return "Hello, world"
}

func main() {
	fmt.Println(Hello())
}
